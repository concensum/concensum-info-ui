import webpack from 'webpack'

export default {
  head: {
    titleTemplate: '%s - concensum.info',
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1, user-scalable=no'},
      {name: 'msapplication-TileColor', content: '#f9423a'},
      {name: 'msapplication-config', content: '/favicons/browserconfig.xml'},
      {name: 'theme-color', content: '#f9423a'}
    ],
    link: [
      { rel: 'apple-touch-icon', sizes: '180x180', href: '/favicons/apple-touch-icon.png' },
      { rel: 'icon', type: 'image/png', sizes: '32x32', href: '/favicons/favicon-32x32.png' },
      { rel: 'icon', type: 'image/png', sizes: '16x16', href: '/favicons/favicon-16x16.png' },
      { rel: 'manifest', href: '/favicons/site.webmanifest' },
      { rel: 'mask-icon', href: '/favicons/safari-pinned-tab.svg', color: '#f9423a' },
      { rel: 'shortcut icon', href: '/favicons/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900|Raleway:300,400,500,600,700,800,900' }
    ],
    script: [
      { src: '/highlight.pack.js' },
      { src: '/highlightInit.js' }
    ],
  },
  css: [
    '@fortawesome/fontawesome-free/css/all.css',
    '@/styles/main.scss',
    '@/styles/highlight.css',
  ],
  render: {
    bundleRenderer: {
      shouldPreload: (file, type) => ['script', 'style', 'font'].includes(type)
    }
  },
  build: {
    extend(config, {isServer}) {
      config.module.rules.push({
        test: /\.ya?ml$/,
        type: 'json',
        use: 'yaml-loader'
      });
      config.plugins.push(new webpack.DefinePlugin({
        'process.env.qtuminfoAPIBase': JSON.stringify(process.env.QTUMINFO_API_BASE
          || process.env[isServer ? 'QTUMINFO_API_BASE_SERVER' : 'QTUMINFO_API_BASE_CLIENT']
          || 'http://localhost:3001/qtuminfo-api/'),
        'process.env.qtuminfoWSBase': JSON.stringify(process.env.QTUMINFO_WS_BASE
          || process.env.QTUMINFO_API_BASE_WS
          || '//localhost:3002/qtuminfo-ws/'),
        'process.env.network': JSON.stringify(process.env.QTUM_NETWORK || 'mainnet')
      }));
    },
    extractCSS: true,
    postcss: {
      features: {
        customProperties: false
      }
    }
  },
  serverMiddleware: ['~/middleware/ip.js'],
  plugins: [
    '~/plugins/components.js',
    '~/plugins/i18n.js',
    '~/plugins/qtum-utils.js',
    {src: '~/plugins/websocket.js', ssr: false}
  ],
  modules:[
    ['nuxt-env', {
      keys: ['NETWORK_NAME']
    }]
  ]
};
