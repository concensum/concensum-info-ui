const powReward = 2457717420000
const nLastPOWBlock = 5000
const halvingInterval = 246375;

function blockRewardByHeight(height) {
  if (height <= nLastPOWBlock) {
    return powReward
  }
  let currentHalving = Math.floor((height - nLastPOWBlock - 1)/ halvingInterval)
  if(currentHalving > 47) {
    return 0
  } else if(currentHalving > 20) {
    return 2e8 >>> (currentHalving - 20)
  } else {
    return 22e8 - (currentHalving * 1e8)
  }
}

export function totalSupplyByHeight(height) {  
  if (height <= nLastPOWBlock) {
    return height * powReward;
  }
  let posHeight = height - nLastPOWBlock
  let curlHalvingInterval = halvingInterval
  let totalPOSReward = 0
  let totalHalvings = Math.floor((posHeight - 1)/ halvingInterval)
  for (let halving = 0; halving <= totalHalvings; halving++) {
    if (halving == totalHalvings) {
      curlHalvingInterval = posHeight - (Math.max((halving - 1), 0) * halvingInterval)
    }
    totalPOSReward += curlHalvingInterval * blockRewardByHeight(halvingInterval * halving + nLastPOWBlock + 1)
  }
  return powReward * nLastPOWBlock + totalPOSReward
}

export function yearlyROI(height, netStakeWeight) {
  // assuming ~144
  return blockRewardByHeight(height) * 219000 / netStakeWeight * 100
}
