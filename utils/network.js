export default {
  mainnet: {
    pubkey: 0x1c,
    pubkeyhash: 0x1c,
    scripthash: 0x32,
    witness_v0_keyhash: 'cc',
    witness_v0_scripthash: 'cc'
  },
  testnet: {
    pubkey: 0x57,
    pubkeyhash: 0x57,
    scripthash: 0x6e,
    witness_v0_keyhash: 'tc',
    witness_v0_scripthash: 'tc'
  }
}[process.env.network || 'mainnet']
